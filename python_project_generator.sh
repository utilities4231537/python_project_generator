#!/bin/bash

if [ -n "$1" ]; then
  echo "Creating Python project $1."

  mkdir $1

  echo "def main():" > $1/app.py
  echo "   print(\"Hello world !\")"  >> $1/app.py
  echo ""                             >> $1/app.py
  echo "if __name__ == \"__main__\":" >> $1/app.py
  echo "   main()"                    >> $1/app.py

  echo "# Add your package dependencies in this file" > $1/requirements.txt
  echo "# package-name==1.0.0" >> $1/requirements.txt

  echo "## This is project $1" >> $1/README.md

  cd $1

  python3 -m venv venv_$1
  source venv_$1/bin/activate
  echo "Project $1 was created successfully."
else
  echo "Please provide a project name as argument"
fi
