## python_project_generator

### Introduction
Bash script that generates a sample Python project with a standard directory structure and a requirements.txt file. It also creates a virtual Python environment and activates it.

### Installation
- Put it in a directory that is included in your PATH (like ~/bin).
- Make it executable : ```chmod 755 ~/bin/python_project_generator.sh```
- Create an alias like "pp" that sources this script
  - Example : `alias pp="source ~/bin/python_project_generator.sh"`

### Usage
Usage with the aforementioned pp alias :
```
pp {project_name}
```

Example :
```shell
~ $ pp new_python_project
``` 

Result
```
Creating Python project new_python_project.
Project new_python_project was created successfully.

(venv_new_python_project) ~/new_python_project $ 
```

